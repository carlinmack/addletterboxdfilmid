import os
from pathlib import Path

TOOL_DATA_DIR = Path(os.environ.get("TOOL_DATA_DIR", ".")).absolute()

family = "wikidata"
mylang = "wikidata"
usernames["wikidata"]["wikidata"] = "AddLetterboxdFilmIdBot"
password_file = TOOL_DATA_DIR / ".password.py"
put_throttle = 1
user_agent_description = "AddLetterboxdFilmIdBot <carlin.mackenzie@gmail.com>"
