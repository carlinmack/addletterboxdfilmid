import argparse
import os
import time
from pathlib import Path

import pywikibot
import requests
from requests.exceptions import HTTPError
from tqdm.rich import tqdm, trange

HEADERS = {"User-Agent": "Add-Letterboxd-ID"}
CUR_DIR = Path(__file__).parent.absolute()
TOOL_DATA_DIR = Path(os.environ.get("TOOL_DATA_DIR", ".")).absolute()


def main(inputFile: str = "", batchSize: int = 100, inputList=[]):
    """From a list of IDs, go through and sequentially output a list of their properties and values."""

    if len(inputList) == 0 and inputFile:
        if not os.path.isfile(TOOL_DATA_DIR / inputFile):
            print(inputFile + " does not exist")
            exit()

        with open(TOOL_DATA_DIR / inputFile) as f:
            inputList = [line.strip() for line in f]
    else:
        inputFile = "all.csv"

    missingFile = "missing-" + inputFile
    invalidFile = "invalid-" + inputFile
    wdqsFile = "wdqs-" + inputFile

    invalidQIDs = readInvalid(invalidFile)

    getMissing(inputList, invalidQIDs, batchSize, missingFile)

    counter = 0
    for i in range(0, 30000, 500):
        start = i
        end = i + 500
        getLetterboxdIds(missingFile, wdqsFile, invalidFile, start, end)

        makeEdits(wdqsFile)

        print(counter)
        counter += 1
        time.sleep(30)  # sleep for 30 secs


def readInvalid(invalidFile):
    if not os.path.isfile(invalidFile):
        return set()
    else:
        with (TOOL_DATA_DIR / invalidFile).open("r") as r:
            return set(line.strip() for line in r)


def getMissing(inputList, invalidQIDs, batchSize, missingFile):
    if len(inputList) > 0:
        # Get file from the source code
        with (CUR_DIR / "fix.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        toFix = []

        for i in trange(0, 1000000, batchSize):
            IDs = inputList[i : i + batchSize]

            if not IDs:
                break

            IDstring = " ".join(["wd:" + q for q in IDs])

            toFix += getData(query, IDstring, invalidQIDs)
    else:
        # Get file from the source code
        with (CUR_DIR / "all.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        toFix = getData(query, filter=invalidQIDs)

    with (TOOL_DATA_DIR / missingFile).open("w") as w:
        for qid, tmdb in toFix:
            w.write(f"{qid},{tmdb}\n")


def getData(query, IDstring="", filter=set()):
    if IDstring:
        data = runQuery(query.format(values=IDstring))
    else:
        data = runQuery(query)

    output = []

    for item in data["results"]["bindings"]:
        QID = item["item"]["value"][31:]
        tmdb = item["tmdb"]["value"]

        if QID not in filter:
            output.append([QID, tmdb])

    return output


def runQuery(query):
    url = "https://query.wikidata.org/sparql"
    params = {"query": query, "format": "json"}
    try:
        response = requests.get(url, params=params, headers=HEADERS)
        return response.json()
    except HTTPError as e:
        print(response.text)
        print(e.response.text)
        print(query)
        return {"results": {"bindings": []}}
    except BaseException as err:
        print(query)
        print(f"Unexpected {err}, {type(err)}")
        raise


def getLetterboxdIds(missingFile, wdqsFile, invalidFile, start, end):
    data = []
    with (TOOL_DATA_DIR / missingFile).open("r") as r:
        for _ in range(start):
            next(r)
        i = 0
        for line in r:
            data.append(line.strip().split(","))
            i += 1
            if i >= 500:
                break

    output = []
    notFound = []
    url = "https://letterboxd.com/tmdb/"
    t = tqdm(total=len(data), miniters=1)

    for qid, tmdb in data:
        response = requests.get(
            url + tmdb + "/",
            headers={
                "User-Agent": "Carlin MacKenzie <carlin.mackenzie@gmail.com> Python script"
            },
        )
        id = response.url[28:-1]
        if id != tmdb:
            output.append([qid, tmdb, id])
        else:
            notFound.append(qid)
        t.update()
        time.sleep(0.1)

    with (TOOL_DATA_DIR / wdqsFile).open("w") as w:
        for qid, tmdb, lb in output:
            w.write(f"{qid},{tmdb},{lb}\n")

    with (TOOL_DATA_DIR / invalidFile).open("a") as a:
        for qid in notFound:
            a.write(f"{qid}\n")


def makeEdits(wdqsFile):
    data = []
    with (TOOL_DATA_DIR / wdqsFile).open("r") as r:
        for line in r:
            data.append(line.strip().split(","))

    site = pywikibot.Site("wikidata", "wikidata")
    site.login()
    repo = site.data_repository()

    t = tqdm(total=len(data), miniters=1)

    for qid, tmdb, lb in data:
        item = pywikibot.ItemPage(repo, qid)
        stringclaim = pywikibot.Claim(repo, "P6127")
        stringclaim.setTarget(lb)

        statedin = pywikibot.Claim(repo, "P854")
        statedin.setTarget("https://letterboxd.com/tmdb/" + tmdb)
        stringclaim.addSources([statedin])

        item.addClaim(stringclaim, summary="Add letterboxd ID")
        t.update()


def timer(tick, msg=""):
    print("--- %s %.3f seconds ---" % (msg, time.time() - tick))
    return time.time()


def defineArgParser():
    """Creates parser for command line arguments"""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )

    parser.add_argument("-f", "--file")

    return parser


if __name__ == "__main__":

    argParser = defineArgParser()
    clArgs = argParser.parse_args()

    tick = time.time()
    main(inputFile=clArgs.file)
    timer(tick)
